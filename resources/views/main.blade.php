<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>PT RAJAWALI PERKASA | SATRYA MANDIRI</title>

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @yield('css')
</head>

<body>

    <header id="header">
        <div class="container">

            <div id="logo" class="pull-left">
                <a href="{{ url('/') }}" class="scrollto">
                    <img src="{{ asset('img/rajawali_prawira_perkasa.png') }}" alt=""
                        style="width:50px;height:auto;">
                </a>
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="{{ $active == 'home' ? 'menu-active' : '' }}"><a href="{{ url('/') }}">Home</a>
                    </li>
                    <li class="{{ $active == 'about' ? 'menu-active' : '' }}"><a href="{{ url('about') }}">About
                            Us</a></li>
                    <li class="{{ $active == 'services' ? 'menu-active' : '' }}"><a
                            href="{{ url('services') }}">Services</a></li>
                    <li class="{{ $active == 'portfolio' ? 'menu-active' : '' }}"><a
                            href="{{ url('portfolio') }}">Portfolio</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    @yield('content')

    <footer id="footer">
        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong>{{ date('Y') }}</strong>. PT. RAJAWALI PERKASA
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>


    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-migrate.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/easing.min.js') }}"></script>
    <script src="{{ asset('js/hoverIntent.js') }}"></script>
    <script src="{{ asset('js/superfish.min.js') }}"></script>
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/sticky.js') }}"></script>
    <script src="{{ asset('js/contactform.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    @yield('js')
</body>

</html>
