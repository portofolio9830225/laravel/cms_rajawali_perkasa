<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $data['active'] = 'home';
        return view('home', $data);
    }

    public function about()
    {
        $data['active'] = 'about';
        return view('about', $data);
    }

    public function services()
    {
        $data['active'] = 'services';
        return view('services', $data);
    }

    public function portfolio()
    {
        $data['active'] = 'portfolio';
        return view('portfolio', $data);
    }
}
