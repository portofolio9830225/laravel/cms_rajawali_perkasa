@extends('main')
@section('content')
    <section id="intro">

        <div class="intro-content">
            <h2>RAJAWALI PRAWIRA PERKASA</h2>
            <div>
                <a href="#about" class="btn-get-started scrollto">Get Started</a>
                <a href="{{ url('services') }}" class="btn-projects scrollto">Our Projects</a>
            </div>
        </div>

        <div id="intro-carousel" class="owl-carousel">
            <div class="item" style="background-image: url('{{ asset('img/slider/1.jpg') }}');"></div>
            <div class="item" style="background-image: url('{{ asset('img/slider/2.jpg') }}');"></div>
            <div class="item" style="background-image: url('{{ asset('img/slider/3.jpg') }}');"></div>
            <div class="item" style="background-image: url('{{ asset('img/slider/4.jpg') }}');"></div>
            <div class="item" style="background-image: url('{{ asset('img/slider/5.jpg') }}');"></div>
        </div>

    </section>

    <main id="main">

        <section id="about" class="wow fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 text-center">
                        <img src="{{ asset('img/rajawali_prawira_perkasa.png') }}" alt=""
                            style="height:250px;width:auto;">
                    </div>

                    <div class="col-lg-6 content">
                        <h2>About Us</h2>
                        <h3>Perusahaan alih daya kami resmi didirikan di Jawa Timur oleh profesional yang berkompeten
                            dan berdedikasi tinggi dalam bidang perusahaan alih daya dan telah mendapatkan ijin
                            operasional sebagai perusahaan alih daya dari Dinas Sosial dan Tenaga Kerja dari Pemerintah.
                        </h3>
                        <a href="{{ url('about') }}" class="btn-about">Selengkapnya...</a>
                    </div>
                </div>

            </div>
        </section>

        <section id="services">
            <div class="container">
                <div class="section-header">
                    <h2>Services</h2>
                </div>

                <div class="row">

                    <div class="col-lg-6">
                        <div class="box wow fadeInLeft">
                            <div class="icon"><i class="fa fa-lock"></i></div>
                            <h4 class="title"><a href="">Pengamanan</a></h4>
                            <p class="description">Jasa yang kami berikan kepada client kami adalah security service.
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="box wow fadeInRight">
                            <div class="icon"><i class="fa fa-trash"></i></div>
                            <h4 class="title"><a href="">Kebersihan</a></h4>
                            <p class="description">Jasa yang kami berikan kepada client kami adalah cleaning
                                service.</p>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="box wow fadeInLeft" data-wow-delay="0.2s">
                            <div class="icon"><i class="fa fa-users"></i></div>
                            <h4 class="title"><a href="">Pendukung</a></h4>
                            <p class="description">Jasa yang kami berikan kepada client kami adalah security cleaning
                                helper, driver, sales force.</p>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="box wow fadeInRight" data-wow-delay="0.2s">
                            <div class="icon"><i class="fa fa-building"></i></div>
                            <h4 class="title"><a href="">Konsultan SDM</a></h4>
                            <p class="description">Jasa yang kami berikan kepada client kami adalah HR consultant.</p>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <section id="call-to-action" class="wow fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 text-center text-lg-left">
                        <h3 class="cta-title">Call To Action</h3>
                        <p class="cta-text"> Kami Siap Melayani Anda kapanpun dan dimanapun Anda berada. Anda Senang
                            Kami juga Senang :)</p>
                    </div>
                    <div class="col-lg-3 cta-btn-container text-center">
                        <a class="cta-btn align-middle" href="https://wa.me/6282225252646" target="_blank">Call To
                            Action</a>
                    </div>
                </div>

            </div>
        </section>

        <section id="contact" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>Contact Us</h2>
                </div>

                <div class="row contact-info">

                    <div class="col-md-4">
                        <div class="contact-address">
                            <i class="fa fa-map-marker"></i>
                            <h3>Address</h3>
                            <address>Komplek bandara juanda T2, Km.1 Sidoarjo, Jawa Timur</address>
                            <address>Jalan Raya Kudus-Pati km. 9 Kudus, Jawa Tengah</address>
                            <address>Emerald District, Bintaro sektor 9, Tangerang Selatan</address>
                            <address>Ruko Rich Palace A1, Banyuwangi</address>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-phone">
                            <i class="fa fa-phone"></i>
                            <h3>Phone Number</h3>
                            <p><a href="tel:+155895548855">0822 2525 2646</a></p>
                            <p><a href="tel:+155895548855">0813 8742 0980</a></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-email">
                            <i class="fa fa-envelope-square"></i>
                            <h3>Email</h3>
                            <p><a href="mailto:rajawaliprawiraperkasa@gmail.com">rajawaliprawiraperkasa@gmail.com</a></p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container mb-4">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.7554747678305!2d112.7649167!3d-7.3812777999999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc0d8b7b8feefea1c!2zN8KwMjInNTIuNiJTIDExMsKwNDUnNTMuNyJF!5e0!3m2!1sid!2sid!4v1666650083278!5m2!1sid!2sid"
                    width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

        </section>

    </main>
@endsection
@section('css')
    <style>
        .btn-about {
            font-family: "Raleway", sans-serif;
            font-size: 15px;
            font-weight: bold;
            letter-spacing: 1px;
            display: inline-block;
            padding: 10px 32px;
            border-radius: 2px;
            transition: 0.5s;
            margin: 10px;
            color: #fff;
            /* background: #0c2e8a; */
            border: 2px solid #0c2e8a;
            color: #0c2e8a;
        }

        .btn-about:hover {
            background: #0c2e8a;
            color: white;
        }
    </style>
@endsection
