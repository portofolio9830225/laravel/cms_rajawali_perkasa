@extends('main')
@section('content')
    <main id="main">

        <section>
            <div class="w-100 position-relative bg-image-about py-3"
                style="background-image: url('{{ asset('img/about-bg.jpg') }}');">
                <div class="d-flex justify-content-center align-items-center text-white mt-3 text-center">
                    <h2 class="font-weight-bold">PORTFOLIO</h2>
                </div>
            </div>
        </section>

        <section class="py-5">
            <div class="container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 mb-3" data-aos="fade-right" data-aos-duration="1000">
                            <div class="section-header">
                                <h2>Toko</h2>
                                <ul>
                                    <li>Metron Elektronika</li>
                                    <li>Point Break Store, Surabaya</li>
                                    <li>Prima Sakti, Surabaya</li>
                                    <li>Bisspark Commercial Area, Sidoarjo</li>
                                    <li>CV Belia, Surabaya</li>
                                    <li>Pergudangan Manukan Wetan, Surabaya</li>
                                    <li>Pergudangan Maspion 4 Romokalisari, Surabaya</li>
                                    <li>Apotik Surya sari Farma, Semarang</li>
                                    <li>Rutraindo Perkasa, Jakarta</li>
                                    <li>Sinar Buana Surya, Surabaya</li>
                                    <li>Pakuwon Mall, Surabaya</li>
                                    <li>Mex Building, Surabaya</li>
                                    <li>Surya Sari Grup, Surabaya</li>
                                    <li>Adinda Tour & Travel, Surabaya</li>
                                    <li>Elang Transport, Sidoarjo</li>
                                    <li>PT. Utama Jaya Bermitra, Jakarta</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3" data-aos="fade-left" data-aos-duration="1000">
                            <div class="section-header">
                                <h2>Pabrik</h2>
                                <ul>
                                    <li>Cellindo Sigma Perkasa, Mojokerto</li>
                                    <li>Zelltech, Mojokerto</li>
                                    <li>Industri Kemasan Semen Gresik, Tuban</li>
                                    <li>Gunawan Fajar Plastindo, Nganjuk</li>
                                    <li>Gunawan Plastik, Sidoarjo</li>
                                    <li>Adi Karya Plastindo, Surabaya</li>
                                    <li>CV Pelangi, Sidoarjo</li>
                                    <li>Mina Jaya Lestari, Sidoarjo</li>
                                    <li>Tirtamas Coldstorindo Logistik, Sidoarjo</li>
                                    <li>Hair Star Indonesia, Sidoarjo</li>
                                    <li>PT. Serim Indonesia, Jepara</li>
                                    <li>PT. Galaxy Anugerah Jaya, Jepara</li>
                                    <li>PT. Surya Energi Bumi, Bondowoso</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3" data-aos="fade-right" data-aos-duration="1000">
                            <div class="section-header">
                                <h2>Usaha Besar</h2>
                                <ul>
                                    <li>Rumah Makan Padang Sederhana, Surabaya</li>
                                    <li>Imperrium Happy Puppy, Surabaya</li>
                                    <li>White House Resto & Cafe</li>
                                    <li>Hotel Tilamas, Sidoarjo</li>
                                    <li>Cantika Martha Tilaar, Surabaya</li>
                                    <li>Dgreen Homestay, Banyuwangi</li>
                                    <li>Bebek Sambel Ijo, Banyuwangi</li>
                                    <li>Warung Desa, Trawas, Mojokerto</li>
                                    <li>Soto Kudus Bu Harti, Kudus</li>
                                    <li>Michelle Beauty Care, Surabaya</li>
                                    <li>RSIA Kendangsari, Surabaya</li>
                                    <li>RSIA Kendangsari Merr, Surabaya</li>
                                    <li>Komplek Ruko dan Restoran Tenggilis, Surabaya</li>
                                    <li>Parahita Laboratorium</li>
                                    <li>Dermagical Aesthetic Clinic, Surabaya</li>
                                    <li>Klinik Amethyst, Surabaya</li>
                                    <li>Tutto Bono Italian Resto, Surabaya</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3" data-aos="fade-left" data-aos-duration="1000">
                            <div class="section-header">
                                <h2>Pemukiman</h2>
                                <ul>
                                    <li>Diraya Regency, Sidoarjo</li>
                                    <li>Sukolilo Dian Regency 1, Surabaya</li>
                                    <li>Sukolilo Dian Regency 2, Surabaya</li>
                                    <li>Sukolilo Dian Regency 3, Surabaya</li>
                                    <li>Apartement Dian Regency, Surabaya</li>
                                    <li>Anugerah Dian Regency, Banjarmasin</li>
                                    <li>Perumahan Purnama Permai, Banjarmasin</li>
                                    <li>Teras Kota Regency, Sidoarjo</li>
                                    <li>Perumahan Candramas 3, Sidoarjo</li>
                                    <li>Perumahan Darmo Sentosa Raya, Surabaya</li>
                                    <li>Galaxy Bumi Permai, Surabaya</li>
                                    <li>Perumahan Mulyosari Utara, Surabaya</li>
                                    <li>Diamond Sky Park, Surabaya</li>
                                    <li>Bavaria Residence, Tangerang</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3" data-aos="fade-right" data-aos-duration="1000">
                            <div class="section-header">
                                <h2>Konstruksi</h2>
                                <ul>
                                    <li>PT. Sinar Bumi Megah, Surabaya</li>
                                    <li>PT. Diparanu Rucitra, Surabaya</li>
                                    <li>PT. Danau Rejeki, Surabaya</li>
                                    <li>PT. Tirta Dan Mitra Pertiwi, Lampung</li>
                                    <li>PT. Waringin, Surabaya</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3" data-aos="fade-left" data-aos-duration="1000">
                            <div class="section-header">
                                <h2>Institusi Pendidikan</h2>
                                <ul>
                                    <li>Mandiri Utama Flying School, Banyuwangi</li>
                                    <li>FKG Unair, Surabaya</li>
                                    <li>Muhammadiyah University, Surabaya</li>
                                    <li>Universitas Perbanas, Surabaya</li>
                                    <li>Universitas Pembangunan Nasional Veteran, Surabaya</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3" data-aos="fade-right" data-aos-duration="1000">
                            <div class="section-header">
                                <h2>Perkebunan</h2>
                                <ul>
                                    <li>CV. Central Intan, Lampung</li>
                                    <li>CV. Lautan Intan, Lampung</li>
                                    <li>CV. Way Raman, Lampung</li>
                                    <li>CV. Sumber Bahagia, Lampung</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3" data-aos="fade-left" data-aos-duration="1000">
                            <div class="section-header">
                                <h2>Transportasi</h2>
                                <ul>
                                    <li>Blimbing Sari Airport, Banyuwangi</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="call-to-action" class="wow fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 text-center text-lg-left">
                        <h3 class="cta-title">Call To Action</h3>
                        <p class="cta-text"> Kami Siap Melayani Anda kapanpun dan dimanapun Anda berada. Anda Senang
                            Kami juga Senang :)</p>
                    </div>
                    <div class="col-lg-3 cta-btn-container text-center">
                        <a class="cta-btn align-middle" href="https://wa.me/6282225252646" target="_blank">Call To
                            Action</a>
                    </div>
                </div>

            </div>
        </section>

        <section id="contact" class="wow fadeInUp">
            <div class="container">
                <div class="section-header">
                    <h2>Contact Us</h2>
                </div>

                <div class="row contact-info">

                    <div class="col-md-4">
                        <div class="contact-address">
                            <i class="fa fa-map-marker"></i>
                            <h3>Address</h3>
                            <address>Komplek bandara juanda T2, Km.1 Sidoarjo, Jawa Timur</address>
                            <address>Jalan Raya Kudus-Pati km. 9 Kudus, Jawa Tengah</address>
                            <address>Emerald District, Bintaro sektor 9, Tangerang Selatan</address>
                            <address>Ruko Rich Palace A1, Banyuwangi</address>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-phone">
                            <i class="fa fa-phone"></i>
                            <h3>Phone Number</h3>
                            <p><a href="tel:+155895548855">0822 2525 2646</a></p>
                            <p><a href="tel:+155895548855">0813 8742 0980</a></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="contact-email">
                            <i class="fa fa-envelope-square"></i>
                            <h3>Email</h3>
                            <p><a href="mailto:rajawaliprawiraperkasa@gmail.com">rajawaliprawiraperkasa@gmail.com</a></p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container mb-4">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.7554747678305!2d112.7649167!3d-7.3812777999999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc0d8b7b8feefea1c!2zN8KwMjInNTIuNiJTIDExMsKwNDUnNTMuNyJF!5e0!3m2!1sid!2sid!4v1666650083278!5m2!1sid!2sid"
                    width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

        </section>

    </main>
@endsection
@section('css')
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <style>
        .bg-image-about {
            background-position: center;
            background-size: cover;
        }
    </style>
@endsection
@section('js')
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
@endsection
